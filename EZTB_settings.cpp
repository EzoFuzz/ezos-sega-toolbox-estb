//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "EZTB_settings.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
Tfrm_ESTB_settings *frm_ESTB_settings;
//---------------------------------------------------------------------------
__fastcall Tfrm_ESTB_settings::Tfrm_ESTB_settings(TComponent* Owner)
	: TForm(Owner)
{
	SET_useNotifications = true;
	SET_alwaysOnTop = false;
    SET_desktopLinks = true;

	STA_roleActive = false;
	STA_roleSelected = ID_Prg;
}
//---------------------------------------------------------------------------
void __fastcall Tfrm_ESTB_settings::btn_cancelClick(TObject *Sender)
{
    // Close Window
    gb_set_general->SetFocus();
    Close();
}
//---------------------------------------------------------------------------
void __fastcall Tfrm_ESTB_settings::chb_drivelClick(TObject *Sender)
{
	// Change Drive Letter Combobox State
	cbb_drivel->Enabled = !cbb_drivel->Enabled;
    if (!chb_drivel->Checked) cbb_drivel->ItemIndex = 0;
}
//---------------------------------------------------------------------------
void __fastcall Tfrm_ESTB_settings::btn_okClick(TObject *Sender)
{
	// Notifications
	SET_useNotifications = chb_notifications->Checked;
	frm_ESTB_main->AddStatus("Settings: ", "Updated.", false);

	// Always on Top
	if (SET_alwaysOnTop != chb_ontop->Checked) {
		SET_alwaysOnTop = chb_ontop->Checked;
		frm_ESTB_main->SetAlwaysOnTop(SET_alwaysOnTop);
	}

	// Desktop Shortcuts
    SET_desktopLinks = chb_desktop->Checked;

	// Drive Letter
	bat_drivel = (chb_drivel->Checked ? cbb_drivel->Text : "S:");

	// Close Window
	gb_set_general->SetFocus();
	Close();
}
//---------------------------------------------------------------------------
void __fastcall Tfrm_ESTB_settings::btn_connectClick(TObject *Sender)
{
	AnsiString exec = frm_ESTB_settings->bat_prefix+"\\ESTB_ssh.bat "+bat_drivel+" "+edt_srv_adr->Text+" "+edt_srv_pwd->Text;

    exec = "cmd.exe /k "+exec;
	char buf[exec.Length()+1];
	strcpy(buf, exec.c_str());

	WinExec(buf, SW_SHOW);
}
//---------------------------------------------------------------------------

