@echo off

:: Reading input variables
set DRV=%1
set SRV=%2
set PWR=%3

:: Variables to change for server setup
set USR=srv_usr

:: Executing SSH
echo "%DRV%\_prg\git\git_bash\bin\bash.exe sshpass -p %PWR% ssh %USR%@%SRV%"
msg %username% "%DRV%\_prg\git\git_bash\bin\bash.exe sshpass -p %PWR% ssh %USR%@%SRV%"
%DRV%\_prg\git\git_bash\bin\bash.exe sshpass -p %PWR% ssh %USR%@%SRV%