#!/bin/bash

DRV="$1"
REP="$2"
USR="$3"
EML="$4"
PWR="$5"
ROL="$6"

# Sanitizing input
REP=$(echo $REP | sed 's/https\?:\/\///')

# Removing Previous
rm -rf "$DRV/_rep/*"
rm -rf "$DRV/_rep/.*"

# Setting up repository
cd "$DRV/_rep" 
git clone http://$USR:$PWR@$REP ./
if [ $? -eq 0 ]; then
    msg /time:2 $USERNAME "SUCCESS: Repository cloned."
else
    msg /time:10 $USERNAME "ERROR: Could not clone repository over HTTP!"
    exit 1
fi

# Setting up branches
case "$ROL" in
    "0")
    WNT_BRANCH="BRANCH_Code"
    ;;

    "1" | "2" | "3")
    WNT_BRANCH="BRANCH_Creative"
    ;;

    "4" | "5")
    WNT_BRANCH="BRANCH_Testing"
    ;;
esac

# Setting up remote settings
git remote rm origin
git remote add origin http://$USR:$PWR@$REP
git fetch origin "$WNT_BRANCH":"$WNT_BRANCH"
git pull origin "$WNT_BRANCH"
git push --set-upstream origin "$WNT_BRANCH"
git config --global user.name  "$USR"
git config --global user.email "$EML"
git config user.name  "$USR"
git config user.email "$EML"

if [ "$ROL" == "0" ]; then
    # Fetch all branches for programmers
    git fetch origin BRANCH_Creative:BRANCH_Creative
    git fetch origin BRANCH_Testing:BRANCH_Testing
fi

git checkout "$WNT_BRANCH"
if [ $? -gt 0 ]; then
    msg /time:10 $USERNAME "ERROR: Branch \"$WNT_BRANCH\" does not exist! Check repostiory settings."
    exit 1
fi
git config --global push.default current
git config --global pull.default current