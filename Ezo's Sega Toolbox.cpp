//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
#include <Vcl.Styles.hpp>
#include <Vcl.Themes.hpp>
USEFORM("Main_ToolchainSetupProg.cpp", frm_ESTB_main);
USEFORM("EZTB_settings.cpp", frm_ESTB_settings);
USEFORM("EZTB_about.cpp", frm_ESTB_about);
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
	HANDLE hMutex = CreateMutex(NULL, FALSE, L"mut_ESTB");
	if (hMutex == NULL)
		ShowMessage(GetLastError());
	else
		if (GetLastError() == ERROR_ALREADY_EXISTS) {
			ShowMessage("Another Toolbox instance is already running!");
            return -1;
        }

	try
	{
		Application->Initialize();
		Application->MainFormOnTaskBar = true;
		Application->Title = "Ezo's Sega Toolbox";
		TStyleManager::TrySetStyle("Carbon");
		Application->CreateForm(__classid(Tfrm_ESTB_main), &frm_ESTB_main);
		Application->CreateForm(__classid(Tfrm_ESTB_settings), &frm_ESTB_settings);
		Application->CreateForm(__classid(Tfrm_ESTB_about), &frm_ESTB_about);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------
