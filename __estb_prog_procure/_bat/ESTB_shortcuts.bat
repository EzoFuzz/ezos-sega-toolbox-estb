@echo off

:: Reading input variables
set DRV=%1
set STA=%2
set ROL=%3
set DSK=%4

:: Other variables for shortcuts
set SRV=192.168.178.163
set HST=srvmddev
set PRT_BRD=8000
set PRT_WIK=9090

if "%STA%"=="true" (goto create) else (goto delete)

:create
goto C_CASE_%ROL%
:C_CASE_0  :: Programmer
    :: Unhide Folders
    attrib -h "%DRV%\_bat\"
    attrib -h "%DRV%\_lib\"
    attrib -h "%DRV%\_png\"
    attrib -h "%DRV%\_tpl\"
    attrib -h "%DRV%\_rep\"

    :: Create Shortcuts
    mklink "%DRV%\Visual Studio Code" "%DRV%\_prg\vscode\Code.exe"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Visual Studio Code" "%DRV%\_prg\vscode\Code.exe")

    mklink "%DRV%\Git Bash" "%DRV%\_prg\_callers\git-bash.bat"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Git Bash" "%DRV%\_prg\_callers\git-bash.bat")

    mklink "%DRV%\Git G" "%DRV%\_prg\_callers\gitg.bat"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Git G" "%DRV%\_prg\_callers\gitg.bat")

    mklink "%DRV%\Local Build - Release" "%DRV%\_prg\_callers\build_release_local.bat"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Local Build - Release" "%DRV%\_prg\_callers\build_release_local.bat")

    mklink "%DRV%\Local Build - Debug" "%DRV%\_prg\_callers\build_debug_local.bat"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Local Build - Debug" "%DRV%\_prg\_callers\build_debug_local.bat")

    mklink "%DRV%\Emu - GensKMod" "%DRV%\_prg\emu\genskmod\gens.exe"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Emu - GensKMod" "%DRV%\_prg\emu\genskmod\gens.exe")

    mklink "%DRV%\Emu - BlastEM" "%DRV%\_prg\emu\blastem\blastem.exe"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Emu - BlastEM" "%DRV%\_prg\emu\blastem\blastem.exe")

    mklink /D "%DRV%\Repository" "%DRV%\_rep\"
    if "%DSK%"=="true" (mklink /D "%userprofile%\Desktop\Repository" "%DRV%\_rep\")

    mklink "%DRV%\Focalboard" "%DRV%\_prg\_url\focalboard.URL"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Focalboard" "%DRV%\_prg\_url\focalboard.URL")

    mklink "%DRV%\Wiki Page" "%DRV%\_prg\_url\wiki.URL"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Wiki Page" "%DRV%\_prg\_url\wiki.URL")
    goto done

:C_CASE_1  :: Artist
    :: Hide Folders
    attrib +h "%DRV%\_bat\"
    attrib +h "%DRV%\_lib\"
    attrib +h "%DRV%\_png\"
    attrib +h "%DRV%\_tpl\"
    attrib +h "%DRV%\_rep\"

    :: Create Shortcuts
    mklink "%DRV%\Aseprite" "%DRV%\_prg\aseprite\aseprite.exe"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Aseprite" "%DRV%\_prg\aseprite\aseprite.exe")

    mklink "%DRV%\Git G" "%DRV%\_prg\_callers\gitg.bat"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Git G" "%DRV%\_prg\_callers\gitg.bat")

    mklink "%DRV%\Local Build - Release" "%DRV%\_prg\_callers\build_release_local.bat"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Local Build - Release" "%DRV%\_prg\_callers\build_release_local.bat")

    mklink "%DRV%\Emu - BlastEM" "%DRV%\_prg\emu\blastem\blastem.exe"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Emu - BlastEM" "%DRV%\_prg\emu\blastem\blastem.exe")

    mklink /D "%DRV%\Repository - Resources" "%DRV%\_rep\res"
    if "%DSK%"=="true" (mklink /D "%userprofile%\Desktop\Repository - Resources" "%DRV%\_rep\res")

    mklink /D "%DRV%\Repository - Game Output" "%DRV%\_rep\out"
    if "%DSK%"=="true" (mklink /D "%userprofile%\Desktop\Repository - Game Output" "%DRV%\_rep\out")

    mklink "%DRV%\Focalboard" "%DRV%\_prg\_url\focalboard.URL"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Focalboard" "%DRV%\_prg\_url\focalboard.URL")

    mklink "%DRV%\Wiki Page" "%DRV%\_prg\_url\wiki.URL"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Wiki Page" "%DRV%\_prg\_url\wiki.URL")
    goto done

:C_CASE_2  :: Musician
    :: Hide Folders
    attrib +h "%DRV%\_bat\"
    attrib +h "%DRV%\_lib\"
    attrib +h "%DRV%\_png\"
    attrib +h "%DRV%\_tpl\"
    attrib +h "%DRV%\_rep\"

    :: Create Shortcuts
    mklink "%DRV%\Deflemask" "%DRV%\_prg\deflemask\deflemask.exe"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Deflemask" "%DRV%\_prg\deflemask\deflemask.exe")

    mklink "%DRV%\Git G" "%DRV%\_prg\_callers\gitg.bat"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Git G" "%DRV%\_prg\_callers\gitg.bat")

    mklink "%DRV%\Local Build - Release" "%DRV%\_prg\_callers\build_release_local.bat"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Local Build - Release" "%DRV%\_prg\_callers\build_release_local.bat")

    mklink "%DRV%\Emu - BlastEM" "%DRV%\_prg\emu\blastem\blastem.exe"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Emu - BlastEM" "%DRV%\_prg\emu\blastem\blastem.exe")

    mklink /D "%DRV%\Repository - Resources" "%DRV%\_rep\res"
    if "%DSK%"=="true" (mklink /D "%userprofile%\Desktop\Repository - Resources" "%DRV%\_rep\res")

    mklink /D "%DRV%\Repository - Game Output" "%DRV%\_rep\out"
    if "%DSK%"=="true" (mklink /D "%userprofile%\Desktop\Repository - Game Output" "%DRV%\_rep\out")

    mklink "%DRV%\Focalboard" "%DRV%\_prg\_url\focalboard.URL"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Focalboard" "%DRV%\_prg\_url\focalboard.URL")

    mklink "%DRV%\Wiki Page" "%DRV%\_prg\_url\wiki.URL"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Wiki Page" "%DRV%\_prg\_url\wiki.URL")
    goto done

:C_CASE_3  :: Level Designer
    :: Hide Folders
    attrib +h "%DRV%\_bat\"
    attrib +h "%DRV%\_lib\"
    attrib +h "%DRV%\_png\"
    attrib +h "%DRV%\_tpl\"
    attrib +h "%DRV%\_rep\"

    :: Create Shortcuts
    mklink "%DRV%\LDTK" "%DRV%\_prg\ldtk\LDtk.exe"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\LDTK" "%DRV%\_prg\ldtk\LDtk.exe")

    mklink "%DRV%\Git G" "%DRV%\_prg\_callers\gitg.bat"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Git G" "%DRV%\_prg\_callers\gitg.bat")

    mklink "%DRV%\Local Build - Release" "%DRV%\_prg\_callers\build_release_local.bat"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Local Build - Release" "%DRV%\_prg\_callers\build_release_local.bat")

    mklink "%DRV%\Emu - BlastEM" "%DRV%\_prg\emu\blastem\blastem.exe"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Emu - BlastEM" "%DRV%\_prg\emu\blastem\blastem.exe")

    mklink /D "%DRV%\Repository - Resources" "%DRV%\_rep\res"
    if "%DSK%"=="true" (mklink /D "%userprofile%\Desktop\Repository - Resources" "%DRV%\_rep\res")

    mklink /D "%DRV%\Repository - Game Output" "%DRV%\_rep\out"
    if "%DSK%"=="true" (mklink /D "%userprofile%\Desktop\Repository - Game Output" "%DRV%\_rep\out")

    mklink "%DRV%\Focalboard" "%DRV%\_prg\_url\focalboard.URL"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Focalboard" "%DRV%\_prg\_url\focalboard.URL")

    mklink "%DRV%\Wiki Page" "%DRV%\_prg\_url\wiki.URL"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Wiki Page" "%DRV%\_prg\_url\wiki.URL")
    goto done

:C_CASE_4  :: Vision Keeper
    :: Hide Folders
    attrib +h "%DRV%\_bat\"
    attrib +h "%DRV%\_lib\"
    attrib +h "%DRV%\_png\"
    attrib +h "%DRV%\_tpl\"
    attrib +h "%DRV%\_rep\"

    :: Create Shortcuts
    mklink "%DRV%\Focalboard" "%DRV%\_prg\_url\focalboard.URL"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Focalboard" "%DRV%\_prg\_url\focalboard.URL")

    mklink "%DRV%\Wiki Page" "start %HST%:%PRT_WIK%"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Wiki Page" "start %HST%:%PRT_WIK%")

    mklink "%DRV%\Emu - BlastEM" "%DRV%\_prg\emu\blastem\blastem.exe"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Emu - BlastEM" "%DRV%\_prg\emu\blastem\blastem.exe")

    mklink /D "%DRV%\Repository - Game Output" "%DRV%\_rep\out"
    if "%DSK%"=="true" (mklink /D "%userprofile%\Desktop\Repository - Game Output" "%DRV%\_rep\out")
    goto done

:C_CASE_5  :: Game Tester
    :: Hide Folders
    attrib +h "%DRV%\_bat\"
    attrib +h "%DRV%\_lib\"
    attrib +h "%DRV%\_png\"
    attrib +h "%DRV%\_tpl\"
    attrib +h "%DRV%\_rep\"

    :: Create Shortcuts
    mklink "%DRV%\Local Build - Release" "%DRV%\_prg\_callers\build_release_local.bat"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Local Build - Release" "%DRV%\_prg\_callers\build_release_local.bat")
        :: REMOVE LATER
    mklink "%DRV%\Git G" "%DRV%\_prg\_callers\gitg.bat"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Git G" "%DRV%\_prg\_callers\gitg.bat")

    mklink "%DRV%\Wiki Page" "%DRV%\_prg\_url\wiki.URL"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Wiki Page" "%DRV%\_prg\_url\wiki.URL")

    mklink "%DRV%\Emu - BlastEM" "%DRV%\_prg\emu\blastem\blastem.exe"
    if "%DSK%"=="true" (mklink "%userprofile%\Desktop\Emu - BlastEM" "%DRV%\_prg\emu\blastem\blastem.exe")

    mklink /D "%DRV%\Repository - Game Output" "%DRV%\_rep\out"
    if "%DSK%"=="true" (mklink /D "%userprofile%\Desktop\Repository - Game Output" "%DRV%\_rep\out")
    goto done


:delete
goto D_CASE_%ROL%
:D_CASE_0  :: Programmer
    echo "DEL RPOG"
    :: Unhide Folders
    attrib -h "%DRV%\_bat\"
    attrib -h "%DRV%\_lib\"
    attrib -h "%DRV%\_png\"
    attrib -h "%DRV%\_tpl\"
    attrib -h "%DRV%\_rep\"

    :: Delete Shortcuts
    del /f "%DRV%\Git Bash"
    if "%DSK%"=="true" (del /f  "%userprofile%\Desktop\Git Bash")

    del /f "%DRV%\Git Bash"
    if "%DSK%"=="true" (del /f  "%userprofile%\Desktop\Git Bash")

    del /f "%DRV%\Git G"
    if "%DSK%"=="true" (del /f  "%userprofile%\Desktop\Git G")

    del /f "%DRV%\Local Build - Release"
    if "%DSK%"=="true" (del /f "%userprofile%\Desktop\Local Build - Release")

    del /f "%DRV%\Local Build - Debug"
    if "%DSK%"=="true" (del /f "%userprofile%\Desktop\Local Build - Debug")

    del /f "%DRV%\Emu - GensKMod"
    if "%DSK%"=="true" (del /f  "%userprofile%\Desktop\Emu - GensKMod")

    del /f "%DRV%\Emu - BlastEM"
    if "%DSK%"=="true" (del /f  "%userprofile%\Desktop\Emu - BlastEM")

    rmdir "%DRV%\Repository"
    if "%DSK%"=="true" (rmdir "%userprofile%\Desktop\Repository")

    del /f "%DRV%\Focalboard"
    if "%DSK%"=="true" (del /f  "%userprofile%\Desktop\Focalboard")

    del /f "%DRV%\Wiki Page"
    if "%DSK%"=="true" (del /f  "%userprofile%\Desktop\Wiki Page")
    goto done

:D_CASE_1  :: Artist
    echo "DEL ART"
    :: Unhide Folders
    attrib -h "%DRV%\_bat\"
    attrib -h "%DRV%\_lib\"
    attrib -h "%DRV%\_png\"
    attrib -h "%DRV%\_tpl\"
    attrib -h "%DRV%\_rep\"

    :: Delete Shortcuts
    del /f "%DRV%\Aseprite"
    if "%DSK%"=="true" (del /f "%userprofile%\Desktop\Aseprite")

    del /f "%DRV%\Git G"
    if "%DSK%"=="true" (del /f "%userprofile%\Desktop\Git G")

    del /f "%DRV%\Local Build - Release"
    if "%DSK%"=="true" (del /f "%userprofile%\Desktop\Local Build - Release")

    del /f "%DRV%\Emu - BlastEM"
    if "%DSK%"=="true" (del /f "%userprofile%\Desktop\Emu - BlastEM")

    rmdir "%DRV%\Repository - Resources"
    if "%DSK%"=="true" (rmdir "%userprofile%\Desktop\Repository - Resources")

    rmdir "%DRV%\Repository - Game Output"
    if "%DSK%"=="true" (rmdir "%userprofile%\Desktop\Repository - Game Output")

    del /f "%DRV%\Focalboard"
    if "%DSK%"=="true" (del /f  "%userprofile%\Desktop\Focalboard")

    del /f "%DRV%\Wiki Page"
    if "%DSK%"=="true" (del /f  "%userprofile%\Desktop\Wiki Page")
    goto done

:D_CASE_2  :: Musician
    echo "DEL MUS"
    :: Unhide Folders
    attrib -h "%DRV%\_bat\"
    attrib -h "%DRV%\_lib\"
    attrib -h "%DRV%\_png\"
    attrib -h "%DRV%\_tpl\"
    attrib -h "%DRV%\_rep\"

    :: Delete Shortcuts
    del /f "%DRV%\Deflemask"
    if "%DSK%"=="true" (del /f "%userprofile%\Desktop\Deflemask")

    del /f "%DRV%\Git G"
    if "%DSK%"=="true" (del /f "%userprofile%\Desktop\Git G")

    del /f "%DRV%\Local Build - Release"
    if "%DSK%"=="true" (del /f "%userprofile%\Desktop\Local Build - Release")

    del /f "%DRV%\Emu - BlastEM"
    if "%DSK%"=="true" (del /f "%userprofile%\Desktop\Emu - BlastEM")

    rmdir "%DRV%\Repository - Resources"
    if "%DSK%"=="true" (rmdir "%userprofile%\Desktop\Repository - Resources")

    rmdir "%DRV%\Repository - Game Output"
    if "%DSK%"=="true" (rmdir "%userprofile%\Desktop\Repository - Game Output")

    del /f "%DRV%\Focalboard"
    if "%DSK%"=="true" (del /f  "%userprofile%\Desktop\Focalboard")

    del /f "%DRV%\Wiki Page"
    if "%DSK%"=="true" (del /f  "%userprofile%\Desktop\Wiki Page")
    goto done

:D_CASE_3  :: Level Designer
    echo "DEL LVD"
    :: Unhide Folders
    attrib -h "%DRV%\_bat\"
    attrib -h "%DRV%\_lib\"
    attrib -h "%DRV%\_png\"
    attrib -h "%DRV%\_tpl\"
    attrib -h "%DRV%\_rep\"

    :: Delete Shortcuts
    del /f "%DRV%\LDTK"
    if "%DSK%"=="true" (del /f "%userprofile%\Desktop\LDTK")

    del /f "%DRV%\Git G"
    if "%DSK%"=="true" (del /f "%userprofile%\Desktop\Git G")

    del /f "%DRV%\Local Build - Release"
    if "%DSK%"=="true" (del /f "%userprofile%\Desktop\Local Build - Release")

    del /f "%DRV%\Emu - BlastEM"
    if "%DSK%"=="true" (del /f "%userprofile%\Desktop\Emu - BlastEM")

    rmdir "%DRV%\Repository - Resources"
    if "%DSK%"=="true" (rmdir "%userprofile%\Desktop\Repository - Resources")

    rmdir "%DRV%\Repository - Game Output"
    if "%DSK%"=="true" (rmdir "%userprofile%\Desktop\Repository - Game Output")

    del /f "%DRV%\Focalboard"
    if "%DSK%"=="true" (del /f  "%userprofile%\Desktop\Focalboard")

    del /f "%DRV%\Wiki Page"
    if "%DSK%"=="true" (del /f  "%userprofile%\Desktop\Wiki Page")
    goto done

:D_CASE_4  :: Vision Keeper
    echo "DEL VIK"
    :: Unhide Folders
    attrib -h "%DRV%\_bat\"
    attrib -h "%DRV%\_lib\"
    attrib -h "%DRV%\_png\"
    attrib -h "%DRV%\_tpl\"
    attrib -h "%DRV%\_rep\"

    :: Delete Shortcuts
    del /f "%DRV%\Focalboard"
    if "%DSK%"=="true" (del /f  "%userprofile%\Desktop\Focalboard")

    del /f "%DRV%\Wiki Page"
    if "%DSK%"=="true" (del /f  "%userprofile%\Desktop\Wiki Page")

    del /f "%DRV%\Emu - BlastEM"
    if "%DSK%"=="true" (del /f "%userprofile%\Desktop\Emu - BlastEM")

    rmdir "%DRV%\Repository - Game Output"
    if "%DSK%"=="true" (rmdir "%userprofile%\Desktop\Repository - Game Output")
    goto done

:D_CASE_5  :: Game Tester
    echo "DEL GAT"
    :: Unhide Folders
    attrib -h "%DRV%\_bat\"
    attrib -h "%DRV%\_lib\"
    attrib -h "%DRV%\_png\"
    attrib -h "%DRV%\_tpl\"
    attrib -h "%DRV%\_rep\"

    :: Delete Shortcuts
    del /f "%DRV%\Local Build - Release"
    if "%DSK%"=="true" (del /f "%userprofile%\Desktop\Local Build - Release")

    del /f "%DRV%\Wiki Page"
    if "%DSK%"=="true" (del /f  "%userprofile%\Desktop\Wiki Page")

    del /f "%DRV%\Emu - BlastEM"
    if "%DSK%"=="true" (del /f "%userprofile%\Desktop\Emu - BlastEM")

    rmdir "%DRV%\Repository - Game Output"
    if "%DSK%"=="true" (rmdir "%userprofile%\Desktop\Repository - Game Output")
    goto done

:done