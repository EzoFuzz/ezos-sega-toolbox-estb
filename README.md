# Ezo's Sega Toolbox - [ESTB]

## What is this?

ESTB is a simple toolbox application, developed to ease the setup of development teams for the Sega Mega Drive / Genesis. It currently supports Windows 7/8/10/11, written in C++ Builder CE 10.4.

![ESTB Program Screenshot](https://gitlab.com/EzoFuzz/ezos-sega-toolbox-estb/-/raw/main/__readme_assets/scrn.png)

I was lucky to be able to cover the creation of a team- & network-based development / creation toolchain for the Sega Mega Drive, as part of my bachelor thesis. Beyond uncovering the limits of team-based development utilizing modern frameworks such as [SGDK](https://github.com/Stephane-D/SGDK), I chose sensible software options for many tasks and streamlined them into a simple toolbox program.

This toolbox is mainly used for mounting of virtual drive directories for the selected team-role, as well as setting up the git version control directories and credentials. The program was put to the test in a 12 hour gamejam, producing a simple run and gun shooter, we named ["Cyborg Jäger"](https://gitlab.com/EzoFuzz/estb-gamejam-cyborg-jaeger).

![Cyborg Jäger Console & Emulator](https://gitlab.com/EzoFuzz/ezos-sega-toolbox-estb/-/raw/main/__readme_assets/cj.png)

## Setting up

To isolate any potential differences and create a working prototype for such a toolchain in a vacuum, local LAN hosting for the git directory and tool applications was used. This, alongside a small self-written wiki and task tracking software was hosted on a RaspberryPi 4, hooked up to a network switch.

![Networking Pi & Switch Setup](https://gitlab.com/EzoFuzz/ezos-sega-toolbox-estb/-/raw/main/__readme_assets/ntw.png)

HTTP git access was configured using Apache2, to separate the user credentials from the system credentials for security and modularity. A bit more information about git setup and role users can be found here:
```
Git Access and Passwords:

Git location:		/srv/git/
Git HTTP Port:		8080
Programmer Users:	usr_prg_1	,,prg**11
						usr_prg_2	,,prg**22
Artist Users:		usr_art_1	,,art**11
						usr_art_2	,,art**22
						[..]
			
Ports of remaining hosted services:

Focalboard:		8000
Mediawiki:		9090
Build Index Page:	7070
```

## Tool procurement
The tools used as part of this toolchain are not all open-source. Some are paid and closed-source. 
Even if all were open-source, redistributing all of these in this fashion would still be a legal gray area, even under GPL-3 or MIT.

As such, every tool and external template folder contains a text file with a link to where the needed tool can be downloaded from. If special instructions for correct application of the downloaded data is necessary, it will be listed inside the text file as well.

Once all tools are in place, the ESTB folder is to be placed as "C:\ESTB\" on the desired development machine. If a different base path for the folder is desired, the environment variables have to be changed accordingly.
