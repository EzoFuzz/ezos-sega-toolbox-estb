//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <System.IOUtils.hpp>
#include "Main_ToolchainSetupProg.h"
#include "EZTB_settings.h"
#include "EZTB_about.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
Tfrm_ESTB_main *frm_ESTB_main;
//---------------------------------------------------------------------------
__fastcall Tfrm_ESTB_main::Tfrm_ESTB_main(TComponent* Owner)
	: TForm(Owner)
{
}


void __fastcall Tfrm_ESTB_main::btn_setClick(TObject *Sender)
{
	frm_ESTB_settings->edt_srv_adr->Text = edt_srv->Text.SubString(0, edt_srv->Text.Pos("/"));
	frm_ESTB_settings->ShowModal();
}

//---------------------------------------------------------------------------

void __fastcall Tfrm_ESTB_main::btn_role_prgClick(TObject *Sender)
{
	SelectRole(frm_ESTB_settings->ID_Prg);
}

void __fastcall Tfrm_ESTB_main::btn_role_artClick(TObject *Sender)
{
	SelectRole(frm_ESTB_settings->ID_Art);
}


void __fastcall Tfrm_ESTB_main::btn_role_musClick(TObject *Sender)
{
	SelectRole(frm_ESTB_settings->ID_Mus);
}

void __fastcall Tfrm_ESTB_main::btn_role_lvdClick(TObject *Sender)
{
	SelectRole(frm_ESTB_settings->ID_LvD);
}

void __fastcall Tfrm_ESTB_main::btn_role_visClick(TObject *Sender)
{
	SelectRole(frm_ESTB_settings->ID_ViK);
}

void __fastcall Tfrm_ESTB_main::btn_role_gmtClick(TObject *Sender)
{
	SelectRole(frm_ESTB_settings->ID_GaT);
}

//---------------------------------------------------------------------------

void Tfrm_ESTB_main::AddStatus(String title, String msg, bool notif)
{
	mem_status->Lines->Add(title+msg);

	if (notif && frm_ESTB_settings->SET_useNotifications) {
	  TNotification *notif;
	  notif = new TNotification;
	  try
	  {
		notif->Name = title;
		notif->Title = title;
		notif->AlertBody = msg;

		frm_ESTB_settings->inv_notif->PresentNotification(notif);
	  }
	  __finally
	  {
		delete notif;
	  }
	}

}

void Tfrm_ESTB_main::ClearStatus()
{
    mem_status->Lines->Clear();
}

void Tfrm_ESTB_main::SetAlwaysOnTop(bool val)
{
	if (val) {
		SetWindowPos(	frm_ESTB_main->WindowHandle,
						HWND_TOPMOST,
						frm_ESTB_main->Left,
						frm_ESTB_main->Top,
						frm_ESTB_main->Width,
						frm_ESTB_main->Height,
						NULL  );
	} else {
		SetWindowPos(	frm_ESTB_main->WindowHandle,
						HWND_NOTOPMOST,
						frm_ESTB_main->Left,
						frm_ESTB_main->Top,
						frm_ESTB_main->Width,
						frm_ESTB_main->Height,
						NULL  );
    }
}

void Tfrm_ESTB_main::SelectRole(int id)
{
	if (frm_ESTB_settings->STA_roleActive)
	{
		// Confirmation
		int res = MessageDlg((frm_ESTB_settings->SET_canClose
							  ? "Role still selected. Deselect before quitting?"
							  : "Are you sure you want to deselect the role?"),
							  mtCustom, TMsgDlgButtons() << mbYes << mbNo, 0)-6;
		if (res > 0) return;

		// Execute Tool unmounting
		ExecuteUnmount();

		// Vars
		frm_ESTB_settings->STA_roleActive = false;

		// Disable Git UI
		for (int i=0; i<6; i++) {
			gb_git->Controls[i]->Enabled = false;
		}
		btn_set->Enabled = true;

		// Re-enable remaining Role buttons
		for (int i=0; i<6; i++) {
			gb_role->Controls[i]->Enabled = true;
		}

		// Username, password and number value
		edt_usr->Text = "";
        edt_pwd->Text = "";
		spe_usrnum->Value = 1;

		// Tray menu role items
		mei_roles_deselect->Enabled = false;
		mei_roles_prog->Enabled = true;
		mei_roles_art->Enabled = true;
		mei_roles_mus->Enabled = true;
		mei_roles_ld->Enabled = true;
		mei_roles_vk->Enabled = true;
		mei_roles_gt->Enabled = true;
	}
	else
	{
		// Vars
		frm_ESTB_settings->STA_roleActive = true;
		frm_ESTB_settings->STA_roleSelected = id;

		// Execute Tool mounting
        ExecuteMount();

		// Enable Git UI
		for (int i=0; i<6; i++) {
			gb_git->Controls[i]->Enabled = true;
		}
		btn_set->Enabled = true;

		// Disable remaining Role buttons
		for (int i=0; i<6; i++) {
			gb_role->Controls[i]->Enabled = false;
		}
		gb_role->Controls[id]->Enabled = true;

		// Username and number value
        edt_usr->Text = frm_ESTB_settings->ID_usr[id];
		spe_usrnum->Value = 1;

		// Tray menu role items
		mei_roles_deselect->Enabled = true;
		mei_roles_prog->Enabled = false;
		mei_roles_art->Enabled = false;
		mei_roles_mus->Enabled = false;
		mei_roles_ld->Enabled = false;
		mei_roles_vk->Enabled = false;
		mei_roles_gt->Enabled = false;
	}
	SetTrayHint();
	//AddStatus("Role: ", frm_ESTB_settings->STA_roleSelected_str, false);
}

// Getter functions
String Tfrm_ESTB_main::GetServerAddress() {
	return edt_srv->Text;
}

String Tfrm_ESTB_main::GetUsername() {
	return (edt_usr->Text+spe_usrnum->Value);
}

String Tfrm_ESTB_main::GetPassword() {
    return edt_pwd->Text;
}

String Tfrm_ESTB_main::GetDesktopFlag() {
	return (frm_ESTB_settings->SET_desktopLinks ? "true" : "false");
}

// Bat execute functions
void Tfrm_ESTB_main::ExecuteMount() {
	AnsiString exec = frm_ESTB_settings->bat_prefix+"\\ESTB_mount.bat "+frm_ESTB_settings->bat_drivel+" "+frm_ESTB_settings->bat_envvar+" "+IntToStr(frm_ESTB_settings->STA_roleSelected)+" "+GetDesktopFlag();
	ExecuteCMD(exec, false);
}

void Tfrm_ESTB_main::ExecuteUnmount() {
	AnsiString exec = frm_ESTB_settings->bat_prefix+"\\ESTB_umount.bat "+frm_ESTB_settings->bat_drivel+" "+frm_ESTB_settings->bat_envvar+" "+IntToStr(frm_ESTB_settings->STA_roleSelected)+" "+GetDesktopFlag();
	ExecuteCMD(exec, false);
}

void Tfrm_ESTB_main::ExecuteGitConf() {
	AnsiString exec = frm_ESTB_settings->bat_prefix+"\\ESTB_git.bat "+frm_ESTB_settings->bat_drivel+" "+GetServerAddress()+" "+GetUsername()+" "+"na "+"\""+GetPassword()+"\""+" "+IntToStr(frm_ESTB_settings->STA_roleSelected);
	ExecuteCMD(exec, false);
}

void Tfrm_ESTB_main::ExecuteCMD(AnsiString exec, bool showWindow) {
	if (!system(NULL)) { AddStatus("Error:", "Could not access CMD.", true);
						 ShowMessage("Error: Could not access CMD.");
						 return; }

    exec = "cmd.exe /k "+exec;
	char buf[exec.Length()+1];
	strcpy(buf, exec.c_str());

	WinExec(buf, (showWindow ? SW_SHOW : SW_HIDE));
}

void Tfrm_ESTB_main::SetTrayHint()
{
	if (frm_ESTB_settings->STA_roleActive) {
	switch (frm_ESTB_settings->STA_roleSelected) {
		case (0):
			frm_ESTB_settings->STA_roleSelected_str = "Programmer";
			break;
		case (1):
			frm_ESTB_settings->STA_roleSelected_str = "Artist";
			break;
		case (2):
			frm_ESTB_settings->STA_roleSelected_str = "Musician";
			break;
		case (3):
			frm_ESTB_settings->STA_roleSelected_str = "Level Designer";
			break;
		case (4):
			frm_ESTB_settings->STA_roleSelected_str = "Vision Keeper";
			break;
		case (5):
			frm_ESTB_settings->STA_roleSelected_str = "Game Tester";
			break;
	}
	} else { frm_ESTB_settings->STA_roleSelected_str = "<None>"; }

	try_trayicon->Hint = ("Ezo's Sega Toolbox | Role: " + frm_ESTB_settings->STA_roleSelected_str);
	try_trayicon->ShowBalloonHint();
}
//---------------------------------------------------------------------------


void __fastcall Tfrm_ESTB_main::img_icoClick(TObject *Sender)
{
    frm_ESTB_about->ShowModal();
}
//---------------------------------------------------------------------------


void __fastcall Tfrm_ESTB_main::FormCloseQuery(TObject *Sender, bool &CanClose)
{
	if (frm_ESTB_settings->SET_canClose)
	{
        CanClose = true;
	} else {
		CanClose = false;
		frm_ESTB_main->Visible = false;
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfrm_ESTB_main::mei_roles_deselectClick(TObject *Sender)
{
    SelectRole(frm_ESTB_settings->STA_roleSelected);
}
//---------------------------------------------------------------------------

void __fastcall Tfrm_ESTB_main::mei_roles_progClick(TObject *Sender)
{
	SelectRole(frm_ESTB_settings->ID_Prg);
}
//---------------------------------------------------------------------------

void __fastcall Tfrm_ESTB_main::mei_roles_artClick(TObject *Sender)
{
	SelectRole(frm_ESTB_settings->ID_Art);
}
//---------------------------------------------------------------------------

void __fastcall Tfrm_ESTB_main::mei_roles_musClick(TObject *Sender)
{
	SelectRole(frm_ESTB_settings->ID_Mus);
}
//---------------------------------------------------------------------------

void __fastcall Tfrm_ESTB_main::mei_roles_ldClick(TObject *Sender)
{
	SelectRole(frm_ESTB_settings->ID_LvD);
}
//---------------------------------------------------------------------------

void __fastcall Tfrm_ESTB_main::mei_roles_vkClick(TObject *Sender)
{
	SelectRole(frm_ESTB_settings->ID_ViK);
}
//---------------------------------------------------------------------------

void __fastcall Tfrm_ESTB_main::mei_roles_gtClick(TObject *Sender)
{
    SelectRole(frm_ESTB_settings->ID_GaT);
}
//---------------------------------------------------------------------------

void __fastcall Tfrm_ESTB_main::mei_aboutClick(TObject *Sender)
{
    frm_ESTB_about->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall Tfrm_ESTB_main::mei_openClick(TObject *Sender)
{
	frm_ESTB_main->Show();
	frm_ESTB_main->BringToFront();
}
//---------------------------------------------------------------------------

void __fastcall Tfrm_ESTB_main::mei_settingsClick(TObject *Sender)
{
    frm_ESTB_settings->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall Tfrm_ESTB_main::mei_quitClick(TObject *Sender)
{
    frm_ESTB_settings->SET_canClose = true;
	Close();
}
//---------------------------------------------------------------------------

void __fastcall Tfrm_ESTB_main::try_trayiconDblClick(TObject *Sender)
{
	frm_ESTB_main->Show();
	frm_ESTB_main->BringToFront();
}
//---------------------------------------------------------------------------


void __fastcall Tfrm_ESTB_main::btn_gitClick(TObject *Sender)
{
	if (edt_srv->Text.Length()==0) { ShowMessage("No server address specified!"); return; }
	if (edt_usr->Text.Length()==0) { ShowMessage("No username specified!"); return; }
	if (edt_pwd->Text.Length()==0) { ShowMessage("No password specified!"); return; }

	if (!TDirectory::IsEmpty(frm_ESTB_settings->bat_drivel+"\\_rep\\")) {
		int res = MessageDlg("Repository is not empty! All unsubmitted changes will be overwritten. Are you sure?", mtCustom, TMsgDlgButtons() << mbYes << mbNo, 0)-6;
		if (res > 0) return;
	}
	ExecuteGitConf();
}
//---------------------------------------------------------------------------

void __fastcall Tfrm_ESTB_main::FormClose(TObject *Sender, TCloseAction &Action)
{
	// Deselect Role if still active
	if (frm_ESTB_settings->STA_roleActive) {
        SelectRole(frm_ESTB_settings->STA_roleSelected);
    }
}
//---------------------------------------------------------------------------

