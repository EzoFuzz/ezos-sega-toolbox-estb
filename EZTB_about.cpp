//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "EZTB_about.h"
#include <shellapi.h>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
Tfrm_ESTB_about *frm_ESTB_about;
//---------------------------------------------------------------------------
__fastcall Tfrm_ESTB_about::Tfrm_ESTB_about(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfrm_ESTB_about::Button1Click(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------
void __fastcall Tfrm_ESTB_about::Button2Click(TObject *Sender)
{
	ShellExecute(0,0, L"https://ezofuzz.me", 0, 0, 0);
}
//---------------------------------------------------------------------------
void __fastcall Tfrm_ESTB_about::Button3Click(TObject *Sender)
{
	ShellExecute(0,0, L"https://gitlab.com/EzoFuzz", 0, 0, 0);
}
//---------------------------------------------------------------------------
