object frm_ESTB_settings: Tfrm_ESTB_settings
  Left = 30
  Top = 6
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Settings'
  ClientHeight = 191
  ClientWidth = 378
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 13
  object gb_set_general: TGroupBox
    Left = 183
    Top = 8
    Width = 185
    Height = 137
    Caption = 'General Settings'
    TabOrder = 0
    object chb_notifications: TCheckBox
      Left = 16
      Top = 24
      Width = 153
      Height = 17
      Caption = 'Enable System notifications'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object chb_desktop: TCheckBox
      Left = 16
      Top = 47
      Width = 153
      Height = 17
      Caption = 'Create Desktop shortcuts'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object chb_ontop: TCheckBox
      Left = 16
      Top = 70
      Width = 97
      Height = 17
      Caption = 'Always on top'
      TabOrder = 2
    end
    object chb_drivel: TCheckBox
      Left = 16
      Top = 93
      Width = 106
      Height = 17
      Caption = 'Specify drive letter'
      TabOrder = 3
      OnClick = chb_drivelClick
    end
    object cbb_drivel: TComboBox
      Left = 128
      Top = 91
      Width = 41
      Height = 21
      Style = csDropDownList
      Enabled = False
      ItemIndex = 0
      TabOrder = 4
      Text = 'S:'
      Items.Strings = (
        'S:'
        'W:'
        'X:'
        'M:'
        'N:'
        'Q:'
        'R:')
    end
  end
  object gb_set_server: TGroupBox
    Left = 8
    Top = 8
    Width = 169
    Height = 137
    Caption = 'Server Settings'
    TabOrder = 1
    object edt_srv_adr: TEdit
      Left = 16
      Top = 22
      Width = 137
      Height = 21
      TabOrder = 0
      TextHint = '( Server Address )'
    end
    object edt_srv_pwd: TEdit
      Left = 16
      Top = 49
      Width = 137
      Height = 21
      PasswordChar = '*'
      TabOrder = 1
      TextHint = '( Server Password )'
    end
    object btn_connect: TButton
      Left = 16
      Top = 84
      Width = 137
      Height = 36
      Caption = 'Access Server'
      TabOrder = 2
      OnClick = btn_connectClick
    end
  end
  object btn_ok: TButton
    Left = 183
    Top = 151
    Width = 66
    Height = 34
    Caption = 'OK'
    TabOrder = 2
    OnClick = btn_okClick
  end
  object btn_cancel: TButton
    Left = 112
    Top = 151
    Width = 65
    Height = 34
    Caption = 'Cancel'
    TabOrder = 3
    OnClick = btn_cancelClick
  end
  object inv_notif: TNotificationCenter
    Left = 32
    Top = 160
  end
end
