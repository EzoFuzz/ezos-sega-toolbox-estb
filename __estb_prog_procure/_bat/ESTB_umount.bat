@echo off

:: Reading input variables
set DRV=%1
set DAT=%2
set ROL=%3
set DSK=%4

:: Checking drive existence
if exist %DRV%\_bat\ goto dounmount
if not exist %DRV%\nul goto done

:: Deleting shortcuts and unmounting folder
:dounmount
powershell -Command "Start-Process cmd -WindowStyle hidden -ArgumentList '/k %DAT%\_bat\ESTB_shortcuts.bat %DRV% false %ROL% %DSK% && subst %DRV% /d && exit' -Verb RunAs"
subst %DRV% /d
goto done

:done