//---------------------------------------------------------------------------

#ifndef EZTB_aboutH
#define EZTB_aboutH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.pngimage.hpp>
//---------------------------------------------------------------------------
class Tfrm_ESTB_about : public TForm
{
__published:	// IDE-managed Components
	TBevel *Bevel1;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TImage *Image1;
	TMemo *Memo1;
	TPanel *Panel1;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfrm_ESTB_about(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfrm_ESTB_about *frm_ESTB_about;
//---------------------------------------------------------------------------
#endif
