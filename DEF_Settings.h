
#ifndef DEF_SET
#define DEF_SET

#include <System.Classes.hpp>
#include "Main_ToolchainSetupProg.h"

// Ref:
Tfrm_ESTB_main *FRM_main = frm_ESTB_main;

// Setting Vars:
bool SET_useNotifications;
bool SET_alwaysOnTop;

#endif