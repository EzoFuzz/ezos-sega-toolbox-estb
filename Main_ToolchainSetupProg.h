//---------------------------------------------------------------------------

#ifndef Main_ToolchainSetupProgH
#define Main_ToolchainSetupProgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.StdCtrls.hpp>
#include <System.Notification.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.Samples.Spin.hpp>
#include <Vcl.Mask.hpp>
//---------------------------------------------------------------------------
class Tfrm_ESTB_main : public TForm
{
__published:	// IDE-managed Components
	TGroupBox *gb_role;
	TButton *btn_role_prg;
	TButton *btn_role_art;
	TButton *btn_role_mus;
	TButton *btn_role_lvd;
	TButton *btn_role_vis;
	TButton *btn_role_gmt;
	TGroupBox *gb_git;
	TEdit *edt_srv;
	TEdit *edt_usr;
	TEdit *edt_pwd;
	TButton *btn_git;
	TButton *btn_set;
	TMemo *mem_status;
	TImage *img_ico;
	TTrayIcon *try_trayicon;
	TPopupMenu *men_traymenu;
	TMenuItem *mei_name;
	TMenuItem *mei_about;
	TMenuItem *mei_open;
	TMenuItem *mei_roles_menu;
	TMenuItem *mei_settings;
	TMenuItem *mei_quit;
	TMenuItem *med_1;
	TMenuItem *med_2;
	TMenuItem *mei_roles_deselect;
	TMenuItem *mei_roles_prog;
	TMenuItem *mei_roles_art;
	TMenuItem *mei_roles_mus;
	TMenuItem *mei_roles_ld;
	TMenuItem *mei_roles_vk;
	TMenuItem *mei_roles_gt;
	TMenuItem *med_roles_1;
	TSpinEdit *spe_usrnum;
	void __fastcall btn_setClick(TObject *Sender);
	void __fastcall btn_role_prgClick(TObject *Sender);
	void __fastcall btn_role_artClick(TObject *Sender);
	void __fastcall btn_role_musClick(TObject *Sender);
	void __fastcall btn_role_lvdClick(TObject *Sender);
	void __fastcall btn_role_visClick(TObject *Sender);
	void __fastcall btn_role_gmtClick(TObject *Sender);
	void __fastcall img_icoClick(TObject *Sender);
	void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
	void __fastcall mei_roles_deselectClick(TObject *Sender);
	void __fastcall mei_roles_progClick(TObject *Sender);
	void __fastcall mei_roles_artClick(TObject *Sender);
	void __fastcall mei_roles_musClick(TObject *Sender);
	void __fastcall mei_roles_ldClick(TObject *Sender);
	void __fastcall mei_roles_vkClick(TObject *Sender);
	void __fastcall mei_roles_gtClick(TObject *Sender);
	void __fastcall mei_aboutClick(TObject *Sender);
	void __fastcall mei_openClick(TObject *Sender);
	void __fastcall mei_settingsClick(TObject *Sender);
	void __fastcall mei_quitClick(TObject *Sender);
	void __fastcall try_trayiconDblClick(TObject *Sender);
	void __fastcall btn_gitClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
	__fastcall Tfrm_ESTB_main(TComponent* Owner);

	void AddStatus(String title, String msg, bool notif);
	void ClearStatus();
	void SetAlwaysOnTop(bool val);

	void SelectRole(int id);
	void SetTrayHint();

	// Getter functions
	String GetServerAddress();
	String GetUsername();
	String GetPassword();
    String GetDesktopFlag();

	// Bat execute functions
	void ExecuteMount();
	void ExecuteUnmount();
	void ExecuteGitConf();
	void ExecuteCMD(AnsiString exec, bool showWindow);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfrm_ESTB_main *frm_ESTB_main;
//---------------------------------------------------------------------------

#endif
