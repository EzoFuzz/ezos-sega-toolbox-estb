@echo off

:: Reading input variables
set DRV=%1
set REP=%2
set USR=%3
set EML=%4
set PWD=%5
set ROL=%6

:: Executing Bash script
powershell -Command "Start-Process cmd -WindowStyle hidden -ArgumentList '/k del /q /s %DRV%\_rep\* && FOR /D %%p IN (%DRV%\_rep\*.*) DO rmdir %%p /s /q && rmdir %DRV%\_rep\.git\ /s /q' -Verb RunAs"
msg /time:4 %username% "Perparing local repository for cloning ..."
timeout /t 4
%DRV%\_prg\git\git_bash\bin\bash.exe %DRV%\_bat\ESTB_git_exec.sh %DRV% %REP% %USR% %EML% %PWD% %ROL%

explorer.exe %DRV%\_rep\
pause