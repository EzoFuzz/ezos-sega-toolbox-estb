//---------------------------------------------------------------------------

#ifndef EZTB_settingsH
#define EZTB_settingsH
//---------------------------------------------------------------------------
#include "Main_ToolchainSetupProg.h"
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <System.Notification.hpp>
//---------------------------------------------------------------------------
class Tfrm_ESTB_settings : public TForm
{
__published:	// IDE-managed Components
	TGroupBox *gb_set_general;
	TGroupBox *gb_set_server;
	TCheckBox *chb_notifications;
	TCheckBox *chb_desktop;
	TCheckBox *chb_ontop;
	TCheckBox *chb_drivel;
	TComboBox *cbb_drivel;
	TEdit *edt_srv_adr;
	TEdit *edt_srv_pwd;
	TButton *btn_connect;
	TButton *btn_ok;
	TButton *btn_cancel;
	TNotificationCenter *inv_notif;

	void __fastcall btn_cancelClick(TObject *Sender);
	void __fastcall chb_drivelClick(TObject *Sender);
	void __fastcall btn_okClick(TObject *Sender);
	void __fastcall btn_connectClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfrm_ESTB_settings(TComponent* Owner);
    bool SET_canClose = false;
	bool SET_useNotifications;
	bool SET_alwaysOnTop;
    bool SET_desktopLinks;

	bool STA_roleActive;
	int  STA_roleSelected;
    String STA_roleSelected_str;
	enum ID_Roles { ID_Prg, ID_Art, ID_Mus, ID_LvD, ID_ViK, ID_GaT};
    String ID_usr[6] = {"usr_prg_", "usr_art_", "usr_mus_", "usr_lvd_", "usr_vik_", "usr_gat_" };

	String bat_envvar   =    "%VAR_ESTB_DAT%";
	String bat_prefix   =    bat_envvar+"\\_bat";
	String bat_drivel   =    "S:";
};
//---------------------------------------------------------------------------
extern PACKAGE Tfrm_ESTB_settings *frm_ESTB_settings;
//---------------------------------------------------------------------------

#endif
