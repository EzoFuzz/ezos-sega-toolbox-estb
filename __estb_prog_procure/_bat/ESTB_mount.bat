@echo off

:: Reading input variables
set DRV=%1
set DAT=%2
set ROL=%3
set DSK=%4

:: Checking drive existence
if exist %DRV%\_bat\ goto notneeded
if not exist %DRV%\nul goto domount
if exist %DRV%\* goto error

:: Mounting folder as virtual drive, call shortcut script
:domount
subst %DRV% %DAT%
powershell -Command "Start-Process cmd -WindowStyle hidden -ArgumentList '/k subst %DRV% %DAT% && %DAT%\_bat\ESTB_shortcuts.bat %DRV% true %ROL% %DSK% && exit' -Verb RunAs"

:: Open mounted folder
if "%DSK%"=="false" (explorer.exe %DRV%)

:: Set Env Vars
setx ESTB_DRV "%DRV%" \m
setx GDK "%DRV%/_lib/SGDK/" \m
setx GDK_WIN "%DRV%\_lib\SGDK\" \m
goto done

:notneeded
Echo ESTB virtual drive already mounted on drive %DRV%
goto done

:error
Echo Drive letter %DRV% already in use by system, please specify different drive letter.

:done